import React from 'react';
import { BrowserRouter,Link,Route,Switch} from 'react-router-dom';
import Preview from './preview'
import Form from './form'
import './App.css';
function App() {
  return(
    <div className="wrapper">
      <h1> Home</h1>
      <BrowserRouter>
      <nav>
        <ul>
        <li><Link to="/form">Form</Link></li>
          <li><Link to="/preview"> Preview</Link></li>
          <li><Link to="/">Home</Link></li>
        </ul>
      </nav>
      <Switch>
        <Route path="/preview">
          <Preview  />
        </Route>
        <Route path="/">
          <h1> hello</h1>
        </Route>
        <Route path="/form">
          <Form/>
        </Route>
      </Switch>
      </BrowserRouter>
    </div>
  )}
  export default App;
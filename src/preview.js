//import React from 'react';
//export default function preview()
//{
//    return(<h2> preview</h2>);
//}
import React, { Component } from 'react'
import ReactDOM from "react-dom";
function validate(name,email,mobile) {
  const errors = [];

  if (name.length === 0) {
    errors.push("Name can't be empty");
  }

  if (email.length < 5) {
    errors.push("Email should be at least 5 charcters long");
  }
  if (email.split("").filter(x => x === "@").length !== 1) {
    errors.push("Email should contain a @");
  }
  if (email.indexOf(".") === -1) {
    errors.push("Email should contain at least one dot");
  }
  if (mobile.length < 12 ) {
    errors.push("Invalid number");
  }
  return errors;
}

class Pre extends Component {
  constructor() {
    super();
    this.state = {
      errors: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const name = ReactDOM.findDOMNode(this._nameInput).value;
    const email = ReactDOM.findDOMNode(this._emailInput).value;
    const mobile = ReactDOM.findDOMNode(this.mobile).value;

    const errors = validate(name, email,mobile);
    if (errors.length > 0) {
      this.setState({ errors });
      return;
    }
    //const { data } = this.props._nameInput
    //const { email } = this.props._emailInput
    //const { mobile } = this.props.mobile
    //window.location = '/preview'; 
    //<Redirect to="/preview"/>
  }

  render() {
    const { errors } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        {errors.map(error => (
          <p key={error}>Error: {error}</p>
        ))}
        Name:<input
          ref={nameInput => (this._nameInput = nameInput)}
          type="text"
          name="username"
          value={this._nameInput}
          placeholder="Name"
        />
        <br></br>
        <br></br>
        Email:<input
          ref={emailInput => (this._emailInput = emailInput)}
          type="text"
          placeholder="Email"
          name="email"
          value={this._emailInput}
        />
        <br></br>
        <br></br>
        number:<input
          ref={mobile => (this.mobile = mobile)}
          type="number"
          placeholder="Enter with country code 91xxxxx"
          name="number"
          value={this.mobile}
        />
        <br></br>
        <br></br>
       <label>
          C 
          <input
            name="val1"
            id="c"
            type="checkbox" />
            <br></br>
          C++
          <input
            name="val2"
            id="cpp"
            type="checkbox" />
            <br></br>
          Python
          <input
            name="val3"
            id = "py"
            type="checkbox" ></input>
        </label>
        <br></br>
        <br></br>
        <input type="radio" id="male" name="gender" value="male"/>
  <label for="male">Male</label><br/>
  <input type="radio" id="female" name="gender" value="female"/>
  <label for="female">Female</label><br/>
<br></br>
        <button type="submit">Preview</button>
      </form>
    );
  }
}
export default Pre